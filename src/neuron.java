import java.util.Random;

public class neuron {

    private Integer O;
    private Integer[] weights = new Integer[12];

    public neuron(){
        for(int i = 0; i < weights.length; i++){
            weights[i] = new Random().nextInt(10) + 1;
        }
        O = new Integer(new Random().nextInt(10) + 1);
    }

    public Integer[] getWeights() {
        return weights;
    }

    public void setWeights(Integer[] i){
        System.arraycopy(i, 0, weights, 0, 12);
    }

    public Integer getO() {
        return O;
    }

    public void setO(Integer o) {
        O = o;
    }

    public int calculateOutput(Integer[] a){
        Integer s = 0;
        for (int i = 0; i < weights.length; i++){
            s = s + (weights[i] * a[i]);
        }
        if(s >= O){
            return 1;
        }
        else{
            return 0;
        }
    }

    public void increaseWeights(int[] a){
        for (int i = 0; i < weights.length; i++){
            weights[i] = weights[i] + a[i];
        }
    }

    public void reduceWeights(Integer[] a){
        for (int i = 0; i < weights.length; i++){
            weights[i] = weights[i] - a[i];
        }
    }

}
