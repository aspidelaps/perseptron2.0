import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import static java.lang.Character.*;

public class main {


    public static void main(String[] args) {
        //Скорость обучения
        double n = 0.5;
        //Создаем наш алфавит, ввиду специфики работы, буква ё является последней в массиве
        char[] alphabet = new char[33];
        char letter = 'А';
        for (int i = 0; i < 33; i++) {
            if (i == 6) {
                alphabet[i] = 'Ё';
                alphabet[i + 1] = 'Ж';
                i = 7;
            } else {
                alphabet[i] = toUpperCase(letter);
            }
            letter = (char) (letter + 1);
        }

        //Создаем 33 нейрона которые будут обрабатывать наш входной сигнал
        neuron[] neurons = new neuron[33];
        for (int i = 0; i < 33; i++) {
            neurons[i] = new neuron();
        }

        //Процесс обучения нейрона

        int count = 0;
        for (int i = 0; i < 33; i++) {
            int[] errorVector = new int[33];
            count++;
            char tmp = alphabet[i];
            for (int j = 0; j < neurons.length; j++) {
                int d = 0;
                if (i == j) {
                    d = 1;
                } else {
                    d = 0;
                }
                int y = neurons[j].calculateOutput(convert(tmp));
                errorVector[j] = d - y;
            }
            if (arrayIsNull(errorVector)) {
                continue;
            } else {
                for (int k = 0; k < neurons.length; k++) {
                    int[] correqt = new int[12];
                    Integer[] x = convert(tmp);
                    for (int p = 0; p < correqt.length; p++) {
                        correqt[p] = errorVector[k] * x[p];
                    }
                    neurons[k].setO(neurons[k].getO() + (-errorVector[k]));
                    neurons[k].increaseWeights(correqt);
                    i = -1;
                }
            }
        }
        System.out.println(count);
        boolean flag = true;
        while (flag) {
            System.out.println("Введите букву:");
            Scanner in = new Scanner(System.in);
            char test =  in.next().toUpperCase().charAt(0);
            for(int i = 0; i < neurons.length; i++){
               int output = neurons[i].calculateOutput(convert(test));
               if(output == 1){
                   System.out.println("Это буква " + alphabet[i] + " я уверена");
               }
            }
        }
    }
    //Конвертер буквы char  в массив входных сигналов
    public static Integer[] convert(char letter) {

        letter = toUpperCase(letter);

        if (compare(letter, 'А') == 0) {
            Integer[] result = {1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1};
            return result;
        }

        if (compare(letter, 'Б') == 0) {
            Integer[] result = {1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1};
            return result;
        }

        if (compare(letter, 'В') == 0) {
            Integer[] result = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
            return result;
        }

        if (compare(letter, 'Г') == 0) {
            Integer[] result = {1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0};
            return result;
        }

        if (compare(letter, 'Д') == 0) {
            Integer[] result = {0, 1, 0, 0, 1, 0, 0, 1, 0, 1, 1, 1};
            return result;
        }

        if (compare(letter, 'Е') == 0) {
            Integer[] result = {1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1};
            return result;
        }

        if (compare(letter, 'Ё') == 0) {
            Integer[] result = {1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
            return result;
        }

        if (compare(letter, 'Ж') == 0) {
            Integer[] result = {1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1};
            return result;
        }

        if (compare(letter, 'З') == 0) {
            Integer[] result = {1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1};
            return result;
        }

        if (compare(letter, 'И') == 0) {
            Integer[] result = {1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1};
            return result;
        }

        if (compare(letter, 'Й') == 0) {
            Integer[] result = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1};
            return result;
        }

        if (compare(letter, 'К') == 0) {
            Integer[] result = {1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1};
            return result;
        }

        if (compare(letter, 'Л') == 0) {
            Integer[] result = {0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1};
            return result;
        }

        if (compare(letter, 'М') == 0) {
            Integer[] result = {1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1};
            return result;
        }

        if (compare(letter, 'Н') == 0) {
            Integer[] result = {1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1};
            return result;
        }


        if (compare(letter, 'О') == 0) {
            Integer[] result = {1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1};
            return result;
        }

        if (compare(letter, 'П') == 0) {
            Integer[] result = {1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1};
            return result;
        }

        if (compare(letter, 'Р') == 0) {
            Integer[] result = {1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0};
            return result;
        }

        if (compare(letter, 'С') == 0) {
            Integer[] result = {1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 1};
            return result;
        }

        if (compare(letter, 'Т') == 0) {
            Integer[] result = {1, 1, 1, 0, 1, 0, 0, 1, 0, 0, 1, 0};
            return result;
        }

        if (compare(letter, 'У') == 0) {
            Integer[] result = {1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1};
            return result;
        }

        if (compare(letter, 'Ф') == 0) {
            Integer[] result = {1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 1, 0};
            return result;
        }

        if (compare(letter, 'Х') == 0) {
            Integer[] result = {1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1};
            return result;
        }

        if (compare(letter, 'Ц') == 0) {
            Integer[] result = {1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1};
            return result;
        }

        if (compare(letter, 'Ч') == 0) {
            Integer[] result = {1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1};
            return result;
        }

        if (compare(letter, 'Ш') == 0) {
            Integer[] result = {1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1};
            return result;
        }

        if (compare(letter, 'Щ') == 0) {
            Integer[] result = {1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1};
            return result;
        }

        if (compare(letter, 'Ъ') == 0) {
            Integer[] result = {1, 1, 0, 0, 1, 0, 0, 1, 1, 0, 1, 1};
            return result;
        }

        if (compare(letter, 'Ы') == 0) {
            Integer[] result = {1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1};
            return result;
        }

        if (compare(letter, 'Ь') == 0) {
            Integer[] result = {0, 1, 0, 0, 1, 0, 0, 1, 1, 0, 1, 1};
            return result;
        }

        if (compare(letter, 'Э') == 0) {
            Integer[] result = {1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 1};
            return result;
        }

        if (compare(letter, 'Ю') == 0) {
            Integer[] result = {1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1};
            return result;
        }

        if (compare(letter, 'Я') == 0) {
            Integer[] result = {0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1};
            return result;
        }

        return null;
    }

    public static boolean arrayIsNull(int[] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == 0) {
                continue;
            } else {
                return false;
            }
        }
        return true;
    }
}
